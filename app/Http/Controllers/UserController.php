<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use DateTime;

class UserController extends Controller
{
    public function login(Request $request){ 


        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user();
            $date = new DateTime();
           $user['last_login'] = $date->format('Y-m-d H:i:s');
            return response()->json(['status' => true,'data' => $user,
            ]);
        } 
        else{ 
            return response()->json(['status' => false]); 
        }
    }
}
