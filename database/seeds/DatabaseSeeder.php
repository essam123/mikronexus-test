<?php

use Illuminate\Database\Seeder;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => ('Essam'),
            'email' => ('essam@gmail.com'),
            'password' => Hash::make('123456'),
            'last_login' => null,
        ]);

        DB::table('users')->insert([
            'name' => ('ali'),
            'email' => ('ali@gmail.com'),
            'password' => Hash::make('123456'),
            'last_login' => null,
        ]);

        DB::table('users')->insert([
            'name' => ('ahsan'),
            'email' => ('ahsan@gmail.com'),
            'password' => Hash::make('123456'),
            'last_login' => null,
        ]);

    }    
}
